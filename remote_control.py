#!/usr/bin/python3
import json
from subprocess import run

server = "voyage.lan"

topic = "kitchen/fridge/CommandControl"


def send_command(cmd, args, func="hal"):
    msg = {"cmd": cmd, "args": args, "func": func}
    msg = json.dumps(msg)
    cmd = ["mosquitto_pub", "-h", server, "-t", topic, "-m", msg]
    print(" ".join(cmd))
    run(cmd)


"""Eval one of the following."""

send_command("light", (True,))
send_command("light", (False,))

send_command("compressor", (True,))
send_command("compressor", (False,))

send_command("deep_freeze", (True,))
send_command("deep_freeze", (False,))

send_command("fan", (True,))
send_command("fan", (False,))

send_command("heaters", (True,))
send_command("heaters", (False,))

send_command("defrost", None, "fridge")

send_command("status", None, "hal")

send_command("reset", None, "fridge")
