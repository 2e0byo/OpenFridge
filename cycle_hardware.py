from machine import Pin
from time import sleep

light = Pin(12, Pin.OUT)
heaters = Pin(14, Pin.OUT)
fan = Pin(27, Pin.OUT)
compressor = Pin(26, Pin.OUT)
deep_freeze = Pin(25, Pin.OUT)


def cycle_hardware(h):
    for i in range(20):
        print(i)
        h.on()
        sleep(1)
        h.off()
        sleep(1)


print("light")
cycle_hardware(light)

print("deep_freeze")
cycle_hardware(deep_freeze)

print("fan")
cycle_hardware(fan)
