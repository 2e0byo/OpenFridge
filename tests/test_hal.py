import pytest

from ..hal.HAL import HAL
from ..machine import Pin


def test_hal_init():
    outputs = [Pin(1, Pin.OUT), Pin(2, Pin.OUT)]
    names = ["light", "fan"]
    h = HAL(outputs, names)
    assert h.engaged == False
