"""Tests for mock machine class."""
from ..machine import Pin, MockPinError
import pytest


def test_init():
    p = Pin(1, Pin.IN, Pin.PULL_UP, value=1)
    q = Pin(1)
    q.init(Pin.IN, Pin.PULL_UP, value=1)

    assert repr(p) == repr(q)
    assert p() == 0
    p.init(Pin.OUT)
    assert p() == 1
    p = Pin(1, Pin.OUT)
    assert p() == 0


def test_write():
    p = Pin(1, Pin.IN)
    assert p() == 0
    p.write(1)
    assert p.value() == 1
    p.write(0)
    assert p() == 0
    p.write("OUT")
    assert p() == 1

    p.init(Pin.OUT)
    with pytest.raises(MockPinError):
        p.write(0)


def test_output_buffer():
    p = Pin(1, Pin.IN)
    p.write(0)
    p.value(1)
    assert p() == 0
    p.init(Pin.OUT)
    assert p() == 1


def test_onoff():
    p = Pin(1, Pin.OUT)
    p.on()
    assert p() == 1
    p.off()
    assert p() == 0
    p.init(Pin.IN)
    p.write(0)
    p.on()
    assert p() == 0
    p.init(Pin.OUT)
    assert p() == 1


def test_irq():
    p = Pin(1)
    with pytest.raises(NotImplementedError):
        p.irq()
