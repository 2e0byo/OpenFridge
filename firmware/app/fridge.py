import logging

import uasyncio as asyncio
from uasyncio import Event

from . import clock, comms, hal
from .HAL.HAL import FridgeError

logger = logging.getLogger(__name__)
logger.addHandler(comms.handler)
logger.addHandler(comms.sh)
logger.setLevel(logging.DEBUG)


freezer_setpoint = -18
fridge_setpoint = 5
hysteresis = 1
mode = "auto"

fridge_updated = Event()
freezer_updated = Event()
fridge_paused = Event()
freezer_paused = Event()
debugging = False


async def fridge_control_loop():
    global fridge_updated
    logger.debug("Started fridge control loop")
    while True:
        while mode == "auto":

            if hal.sensors["fridge_temp"] < fridge_setpoint - hysteresis:
                if debugging:
                    logger.debug("Fridge under temperature")
                hal.fridge_freezer.refridgerating = False
            elif hal.sensors["fridge_temp"] > fridge_setpoint + hysteresis:
                if debugging:
                    logger.debug("Fridge over temperature")
                try:
                    hal.fridge_freezer.refridgerating = True
                    if hal.sensors["ext_temp"] < 15:
                        logger.info("Deep freezing due to external temperature")
                        hal.fridge_freezer.set_state("deep_freeze", True)
                except FridgeError:
                    if debugging:
                        logger.debug("FridgeError raised")
                    pass

            fridge_updated.set()
            await asyncio.sleep(3)

        logger.debug("Switching fridge to manual control")
        fridge_paused.set()

        while mode != "auto":
            fridge_updated.set()
            await asyncio.sleep(3)
        fridge_paused.set()


async def freezer_control_loop():
    global freezer_updated
    logger.debug("Started freezer control loop")
    while True:
        while mode == "auto":

            if hal.sensors["freezer_temp"] < freezer_setpoint - hysteresis:
                if debugging:
                    logger.debug("Freezer under temperature")
                hal.fridge_freezer.freezing = False
            elif (
                hal.sensors["freezer_temp"] > (freezer_setpoint + hysteresis)
                and not hal.fridge_freezer.freezing
            ):
                if debugging:
                    logger.debug("Freezer over temperature")
                try:
                    hal.fridge_freezer.freezing = True
                    start_rapid_freeze_timer()
                except FridgeError:
                    if debugging:
                        logger.debug("FridgeError raised")
                    pass

            freezer_updated.set()
            await asyncio.sleep(3)

        logger.debug("Switching freezer to manual control")
        freezer_paused.set()

        while mode != "auto":
            freezer_updated.set()
            await asyncio.sleep(3)
        freezer_paused.set()


async def alarm_loop():
    """Stop things getting very bad."""
    while True:
        while mode == "auto":
            if (
                hal.sensors["freezer_temp"] > freezer_setpoint + 5
                or hal.sensors["fridge_temp"] > fridge_setpoint + 5
            ):
                hal.fridge_freezer.defrosting = False
                hal.alarm("overtemp", hal.alarm.SOUNDING)
            else:
                hal.alarm("overtemp", hal.alarm.OFF)
            await asyncio.sleep(10)

        while mode != "auto":
            await asyncio.sleep(1)


async def _defrost():
    i = 0
    while i < 60:
        try:
            hal.fridge_freezer.defrosting = True
            break
        except FridgeError:
            await asyncio.sleep(10)
    if not hal.fridge_freezer.defrosting:
        logger.info("Failed to start defrosting: timed out waiting")
        return

    while not hal.fridge_freezer.achieved_state("heaters"):
        await asyncio.sleep(10)

    # defrost till element reaches 10C or 120 minutes is up
    for i in range(60 * 12):
        if hal.sensors["cooler_temp"] >= 2:
            break
        await asyncio.sleep(10)
    await asyncio.sleep(60 * 5)  # reduce thermal stress
    hal.fridge_freezer.defrosting = False


defrost_hours = [2, 14]  # don't use 0 as the rtc initialises to that


async def defrost_loop():
    """See if the RTC time is such that a defrost is a good idea."""
    while True:
        nowh, nowm = clock.rtc.datetime()[4:6]
        if nowm < 5 and nowh in defrost_hours:  # defrost lasts at least 5 minutes
            if not hal.fridge_freezer.defrosting:
                await _defrost()

        await asyncio.sleep(60)


async def _panic_defrost_timer():
    """
    Panic if we haven't managed to go 2 degrees under in fifteen minutes.

    Might need adjusting.
    """
    logger.debug("Starting panic defrost timer")
    start = hal.sensors["freezer_temp"]
    count = 0
    while True:  # break if we've stopped elsewhere
        if not hal.fridge_freezer.freezing:
            return
        count += 1
        await asyncio.sleep(60)
        if count >= 15:
            break
    if start - hal.sensors["freezer_temp"] < 2:
        logger.info("Panic defrosting!")
        await _defrost()
    else:
        logger.debug("Exiting panic defrost timer with nothing to do.")


def start_panic_defrost_timer():
    asyncio.get_event_loop().create_task(_panic_defrost_timer())


async def _rapid_freeze_timer():
    if not hal.fridge_freezer.freezing:
        if debugging:
            logger.debug("Returning as not freezing")
        return

    logger.debug("Starting rapid freeze timer.")
    if hal.sensors["ext_temp"] < 15:
        logger.info("Deep freezing due to external temperature")
        hal.fridge_freezer.set_state("deep_freeze", True)
        return

    if hal.sensors["freezer_temp"] > -10:
        logger.info("Deep freezing as freezer too warm")
        hal.fridge_freezer.set_state("deep_freeze", True)
        return

    start = hal.sensors["freezer_temp"]
    await asyncio.sleep(60 * 20)
    if not hal.fridge_freezer.freezing:
        logger.debug("Exiting deep freezer timer as no longer cooling.")
        return

    if start - hal.sensors["freezer_temp"] < 1:
        logger.info("Deep freezing as freezing slowly")
        hal.fridge_freezer.set_state("deep_freeze", True)
        return
    else:
        logger.debug("Exiting deep freeze timer with nothing to do.")


def start_rapid_freeze_timer():
    asyncio.get_event_loop().create_task(_rapid_freeze_timer())


async def ext_wdt_feed():
    while True:
        await fridge_updated.wait()
        fridge_updated.clear()
        await freezer_updated.wait()
        freezer_updated.clear()
        hal.wdt_feed()
        await asyncio.sleep(3)


def start():
    pass


def manual_defrost():
    """Manual defrost routine."""
    asyncio.get_event_loop().create_task(_defrost())
    print("Called manual defrost")


def init(loop):
    loop.create_task(ext_wdt_feed())
    loop.create_task(fridge_control_loop())
    loop.create_task(freezer_control_loop())
    loop.create_task(alarm_loop())
    loop.create_task(defrost_loop())
