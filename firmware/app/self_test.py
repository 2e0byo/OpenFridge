from .hal import roms, fridge_rom, freezer_rom, cooler_rom, ext_rom, FridgeError
from . import hal
from sys import print_exception
import uasyncio as asyncio
import machine
import gc

errors = []


def log_print(*args):
    print(*args)
    with open("/static/test.log", "a") as f:
        f.write("{}\n".format(" ".join(args)))


class HardwareTestError(Exception):
    pass


async def test_hal_output(output, masking=False):
    log_print("Testing {}".format(output))
    log_print(str(hal.fridge_freezer._state))
    hal.fridge_freezer.set_state(output, True)
    assert hal.fridge_freezer(output), "Failed to set state"
    await asyncio.sleep(2)
    if not hal.fridge_freezer.achieved_state(output):
        raise HardwareTestError("Unable to set {}.".format(output))
    if not masking:
        assert hal.fridge_freezer._outputs[output](), "Output not physically turned on"

    hal.fridge_freezer.set_state(output, False)
    assert not hal.fridge_freezer(output), "Failed to unset state"
    await asyncio.sleep(2)
    if not hal.fridge_freezer.achieved_state(output):
        raise HardwareTestError("Unable to unset {}.".format(output))
    if not masking:
        assert not hal.fridge_freezer._outputs[
            output
        ](), "Output not physically turned on"


async def run_test(testfn, name):
    log_print("----Running test {}----".format(name))
    gc.collect()
    try:
        await testfn()
        log_print("Test completed")
    except Exception as e:
        log_print("Test {} raised exception".format(name))
        print_exception(e)
        with open("/static/test.log", "a") as f:
            print_exception(e, f)
        errors.append(e)


async def test_for_roms():
    assert fridge_rom in roms, "No fridge rom"
    assert freezer_rom in roms, "No freezer rom"
    assert cooler_rom in roms, "No cooler rom"
    assert ext_rom in roms, "No ext rom"


async def test_alarms():
    # test alarm
    log_print("Door Open Alarm")
    hal.alarm("door_open", hal.Alarm.SOUNDING)
    await asyncio.sleep(3)
    assert hal.alarm("door_open") == hal.Alarm.SOUNDING, "Door alarm not set"

    log_print("Over Temp Alarm")
    hal.alarm("overtemp", hal.Alarm.SOUNDING)
    await asyncio.sleep(3)
    assert hal.alarm("door_open") == hal.Alarm.SOUNDING, "Door alarm not set"
    assert hal.alarm("overtemp") == hal.Alarm.SOUNDING, "Overtemp alarm not set"

    log_print("Silencing Door Open")
    hal.alarm("door_open", hal.alarm.SILENCED)
    await asyncio.sleep(3)
    assert hal.alarm("door_open") == hal.Alarm.SILENCED, "Door alarm not silienced"
    assert hal.alarm("overtemp") == hal.Alarm.SOUNDING, "Overtemp alarm not set"

    log_print("Retriggering Door Open (should not sound)")
    hal.alarm("door_open", hal.alarm.SOUNDING)
    await asyncio.sleep(3)
    assert hal.alarm("door_open") == hal.Alarm.SILENCED, "Door alarm not silienced"
    assert hal.alarm("overtemp") == hal.Alarm.SOUNDING, "Overtemp alarm not set"

    log_print("cancelling all")
    hal.alarm.cancel_all()
    assert hal.alarm("door_open") == hal.Alarm.OFF, "Door alarm not off"
    assert hal.alarm("overtemp") == hal.Alarm.OFF, "Overtemp alarm not off"


async def test_hal():
    log_print("Resetting controller")

    hal.fridge_freezer.engaged = False
    hal.fridge_freezer.reset()
    hal.fridge_freezer.engaged = True
    log_print(repr(hal.fridge_freezer))

    assert hal.fridge_freezer.engaged, "Hal not engaged"
    assert hal.fridge_freezer.debugging, "Hal not in debugging mode."
    assert not any(
        v() for k, v in hal.fridge_freezer._outputs.items()
    ), "Hal doing stuff {}".format(hal.fridge_freezer._outputs)
    assert not any(
        v for k, v in hal.fridge_freezer._state.items()
    ), "Hal doing stuff {}".format(hal.fridge_freezer._state)
    assert not hal.fridge_freezer.freezing, "Freezing"
    assert not hal.fridge_freezer.refridgerating, "Refridgerating"
    assert not hal.fridge_freezer.defrosting, "Defrosting"

    log_print("Conflicts:")
    log_print(str(hal.fridge_freezer._conflicts))

    log_print("Downtime:")
    log_print(str(hal.fridge_freezer._downtimes))

    await test_hal_output("fan", masking=True)
    await test_hal_output("heaters")
    await test_hal_output("deep_freeze")

    log_print("Testing downtime")
    hal.fridge_freezer.set_state("compressor", True)
    await asyncio.sleep(2)
    assert hal.fridge_freezer.achieved_state(
        "compressor"
    ), "Failed to turn compressor on"
    hal.fridge_freezer.set_state("compressor", False)
    await asyncio.sleep(2)
    assert hal.fridge_freezer.achieved_state(
        "compressor"
    ), "Failed to turn compressor off"
    await asyncio.sleep(2)
    assert hal.fridge_freezer._downtimes["compressor"], "No downtime set"
    assert hal.fridge_freezer._sleeping["compressor"], "Compressor not sleeping"
    hal.fridge_freezer.set_state("compressor", True)
    await asyncio.sleep(2)
    assert not hal.fridge_freezer.achieved_state("compressor"), "Downtime ignored"
    hal.fridge_freezer.engaged = False
    hal.fridge_freezer.engaged = True
    assert not hal.fridge_freezer._sleeping["compressor"], "Failed to clear sleeping."
    hal.fridge_freezer.set_state("compressor", True)
    await asyncio.sleep(2)
    assert hal.fridge_freezer.achieved_state(
        "compressor"
    ), "Failed to turn compressor on after cancelling sleeping"

    print("Testing downtime elapse")
    downtime = hal.fridge_freezer._downtimes["compressor"]
    hal.fridge_freezer.engaged = False
    hal.fridge_freezer.reset()
    hal.fridge_freezer._downtimes["compressor"] = 10
    hal.fridge_freezer.engaged = True

    hal.fridge_freezer.set_state("compressor", True)
    await asyncio.sleep(2)
    assert hal.fridge_freezer.achieved_state(
        "compressor"
    ), "Failed to turn compressor on"
    hal.fridge_freezer.set_state("compressor", False)
    await asyncio.sleep(2)
    assert hal.fridge_freezer._downtimes["compressor"], "No downtime set"
    assert hal.fridge_freezer._sleeping["compressor"], "Compressor not sleeping"
    hal.fridge_freezer.set_state("compressor", True)
    await asyncio.sleep(2)
    assert not hal.fridge_freezer.achieved_state("compressor"), "Downtime ignored"
    await asyncio.sleep(10)
    assert not hal.fridge_freezer._sleeping["compressor"], "Compressor still sleeping"
    assert hal.fridge_freezer.achieved_state("compressor"), "Downtime failed to elapse"

    hal.fridge_freezer._downtimes["compressor"] = downtime

    log_print("Testing conflicts")
    hal.fridge_freezer.set_state("heaters", True)
    await asyncio.sleep(2)
    assert not hal.fridge_freezer.achieved_state(
        "heaters"
    ), "Managed to turn on heaters"
    hal.fridge_freezer.set_state("compressor", False)
    await asyncio.sleep(5)
    assert hal.fridge_freezer.achieved_state("heaters"), "Failed to turn on heaters"
    hal.fridge_freezer.set_state("deep_freeze", True)
    await asyncio.sleep(2)
    assert not hal.fridge_freezer.achieved_state(
        "deep_freeze"
    ), "Turned on deep freeze whilst heaters on"
    hal.fridge_freezer.set_state("heaters", False)
    await asyncio.sleep(2)
    assert hal.fridge_freezer.achieved_state("heaters"), "Failed to turn off heaters"

    log_print("Test over, resetting.")
    hal.fridge_freezer.engaged = False
    hal.fridge_freezer.reset()
    hal.fridge_freezer.engaged = True


async def test_fridge_hal():
    log_print("Resetting controller")

    hal.fridge_freezer.engaged = False
    hal.fridge_freezer.reset()
    hal.fridge_freezer.engaged = True
    log_print(repr(hal.fridge_freezer))

    log_print("Testing refridgeration")
    hal.fridge_freezer.refridgerating = True
    await asyncio.sleep(5)
    assert hal.fridge_freezer.refridgerating, "Refridgerating off"
    assert hal.fridge_freezer.get_state(
        "compressor"
    ), "Didn't try to turn compressor on"

    log_print("Testing freezing")
    hal.fridge_freezer.freezing = True
    await asyncio.sleep(5)
    assert hal.fridge_freezer.freezing, "Freezing off"
    assert hal.fridge_freezer.get_state(
        "compressor"
    ), "Didn't try to turn compressor on"
    assert hal.fridge_freezer.get_state("fan"), "Didn't try to turn fan on"
    assert hal.fridge_freezer.achieved_state("fan"), "Failed to turn on fan"

    log_print("Testing defrost inhibit")
    try:
        hal.fridge_freezer.defrosting = True
        raise HardwareTestError("Managed to turn on defrosting when freezing")
    except FridgeError:
        log_print("successfully inhibited")
    hal.fridge_freezer.freezing = False
    hal.fridge_freezer.refridgerating = False
    hal.fridge_freezer.defrosting = True
    assert hal.fridge_freezer.defrosting, "Failed to start defrosting"
    await asyncio.sleep(2)
    assert hal.fridge_freezer("heaters"), "Didn't try to turn on heaters"
    assert hal.fridge_freezer.achieved_state("heaters"), "Failed to turn on heaters"
    hal.fridge_freezer.defrosting = False
    assert not hal.fridge_freezer.defrosting, "Failed to stop defrosting"
    await asyncio.sleep(2)
    assert not hal.fridge_freezer("heaters"), "Didn't try to turn off heaters"
    assert hal.fridge_freezer.achieved_state("heaters"), "Failed to turn off heaters"

    log_print("Test over, resetting")
    hal.fridge_freezer.engaged = False
    hal.fridge_freezer.reset()
    hal.fridge_freezer.engaged = True


async def test_setpoints():
    assert not hal.fridge_freezer.freezing, "Already freezing!"
    assert not hal.fridge_freezer.refridgerating, "Already refridgerating!"
    assert not hal.fridge_freezer.defrosting, "Already defrosting!"

    print("Mocking temperatures.")
    hal.mock_temps = True
    await hal.mocking_temps.wait()
    hal.mocking_temps.clear()

    log_print("setting temps which should cause nothing to engage")
    hal.sensors["fridge_temp"] = 0
    hal.sensors["freezer_temp"] = -20
    hal.sensors["cooler_temp"] = -20
    hal.sensors["ext_temp"] = 20

    log_print("Turning fridge back on")
    from . import fridge

    fridge.mode = "auto"
    fridge.debugging = True
    await fridge.fridge_paused.wait()
    fridge.fridge_paused.clear()
    await fridge.freezer_paused.wait()
    fridge.freezer_paused.clear()
    log_print("Auto control enabled")
    await asyncio.sleep(5)
    assert not hal.fridge_freezer.freezing, "Started freezing!"
    assert not hal.fridge_freezer.refridgerating, "Started refridgerating!"
    assert not hal.fridge_freezer.defrosting, "Started defrosting!"
    hal.sensors["fridge_temp"] = 10
    await asyncio.sleep(5)
    assert hal.fridge_freezer.refridgerating, "Didn't start refridgerating"
    hal.sensors["freezer_temp"] = 0
    await asyncio.sleep(5)
    assert hal.fridge_freezer.freezing, "Didn't start freezing"
    if hal.alarm("overtemp") != hal.alarm.SOUNDING:
        await asyncio.sleep(15)
    assert hal.alarm("overtemp") == hal.alarm.SOUNDING, "Overtemp alarm didn't start"
    hal.alarm("overtemp", hal.alarm.SILENCED)
    assert hal.fridge_freezer("deep_freeze"), "Didn't start deep freezing"
    # hal.sensors["ext_temp"] = 0
    # await asyncio.sleep(5)
    # assert hal.fridge_freezer("deep_freezE"), "Didn't start Deep Freezing"
    hal.sensors["fridge_temp"] = 2
    await asyncio.sleep(4)
    assert not hal.fridge_freezer.refridgerating, "Didn't stop refridgerating"
    hal.sensors["freezer_temp"] = -20
    await asyncio.sleep(4)
    assert not hal.fridge_freezer.freezing, "Didn't stop freezing"
    assert not hal.fridge_freezer("deep_freeze"), "Didn't turn off deep_freeze"
    if hal.alarm("overtemp") != hal.alarm.OFF:
        await asyncio.sleep(15)
    assert hal.alarm("overtemp") == hal.alarm.OFF, "Overtemp alarm didn't stop"
    hal.alarm.cancel_all()
    hal.mock_temps = False
    fridge.debugging = False
    fridge.mode = "manual"


async def _self_test():
    with open("/static/test.log", "w") as f:
        f.write("")

    log_print("====Beginning self test routine.====")
    log_print("Switching off auto control")
    from . import fridge

    fridge.mode = "manual"
    await fridge.fridge_paused.wait()
    fridge.fridge_paused.clear()
    await fridge.freezer_paused.wait()
    fridge.freezer_paused.clear()
    log_print("Auto control is paused")

    log_print("Resetting hal")
    hal.fridge_freezer.engaged = False
    hal.fridge_freezer.debugging = True
    hal.fridge_freezer.reset()
    hal.fridge_freezer.engaged = True
    log_print("Waiting for everything to stop")
    await asyncio.sleep(4)
    log_print("masking fan")
    hal.fridge_freezer.masked_hardware.append("fan")

    await run_test(test_for_roms, "Testing for roms")
    await run_test(test_hal, "Testing HAL")
    await run_test(test_fridge_hal, "Testing fridge HAL")
    await run_test(test_alarms, "Testing alarms")
    await run_test(test_setpoints, "Testing setpoints")

    log_print("Resetting hal")
    hal.fridge_freezer.engaged = False
    hal.fridge_freezer.reset()
    log_print("unmasking fan")
    hal.fridge_freezer.masked_hardware.remove("fan")
    hal.fridge_freezer.debugging = False
    hal.fridge_freezer.engaged = True

    hal.mock_temps = False

    fridge.mode = "auto"

    if errors:
        log_print("\n\n---Errors:---\n\n")
        for e in errors:
            print_exception(e)
            with open("/static/test.log", "a") as f:
                print_exception(e, f)
        log_print("\n\n")

    else:
        log_print("\n\n---Success---\n\n")
        log_print("All tests passed\n\n")

    log_print("====Testing completed====")

    print("\n\nWill reboot in 60s")
    asyncio.get_event_loop().create_task(reboot_later())
    gc.collect()


async def reboot_later():
    await asyncio.sleep(60)
    print("Rebooting....")
    machine.reset()


def run_tests():
    asyncio.get_event_loop().create_task(_self_test())
