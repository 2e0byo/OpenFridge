import ds18x20
import onewire
from micropython import const


class OneWire(onewire.OneWire):
    def normal_pull(self):
        self.pin.init(self.pin.OPEN_DRAIN, self.pin.PULL_UP, value=1)

    def strong_pull(self):
        # pulls the 1-wire pin actively high. Use a small PTC as protection...
        self.pin.init(self.pin.OUT, value=1)

    def power_off_par(self):
        # may protect against (or even clear) latch-up.
        # If the pull-up resistor is connected to VCC, make sure it can withstand it.
        # To save power: Connect external pull-up to a seperate GPIO.
        self.pin.init(self.pin.OPEN_DRAIN, pull=None, value=0)

    def reset(self, required=False):
        self.normal_pull()  # needed to restore Open Drain Output
        return super().reset(required)


class DS18X20(ds18x20.DS18X20):
    _CONVERT = const(0x44)

    def normal_pull(self):
        self.ow.normal_pull()

    def convert_temp_par(self):  # 0 => no strong. Neg => pull low. Pos => pull high.
        self.ow.reset(True)  # This will clear any remaining strong pull
        self.ow.writebyte(self.ow.SKIP_ROM)
        self.ow.strong_pull()  # Not very nice but works!
        self.ow.writebyte(_CONVERT)
