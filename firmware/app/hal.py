import logging
from time import sleep_ms
from sys import print_exception

try:
    import uasyncio as asyncio
except ImportError:
    import asyncio
from machine import ADC, PWM, WDT, Pin

from . import clock, comms, config, fridge
from .config import cooler_rom, ext_rom, freezer_rom, fridge_rom
from .par import DS18X20, OneWire
from primitives.pushbutton import Pushbutton
from primitives.switch import Switch
from .HAL.HAL import FridgeFreezerHal, FridgeError
from .HAL.alarm import Alarm

logger = logging.getLogger(__name__)
logger.addHandler(comms.handler)
logger.addHandler(comms.sh)
logger.setLevel(logging.DEBUG)

mock_temps = False
mocking_temps = asyncio.Event()

status_led = Pin(13, Pin.OUT)
external_wdt = Pin(16, Pin.OUT)

ow = OneWire(Pin(22))
sens = DS18X20(ow)
roms = sens.scan()


door_switch_pin = Pin(2, Pin.IN, Pin.PULL_UP)
enter_pin = Pin(18, Pin.IN, Pin.PULL_UP)
plus_pin = Pin(17, Pin.IN, Pin.PULL_UP)
minus_pin = Pin(15, Pin.IN, Pin.PULL_UP)
light = Pin(12, Pin.OUT)
heaters = Pin(14, Pin.OUT)
fan = Pin(27, Pin.OUT)
compressor = Pin(26, Pin.OUT)
deep_freeze = Pin(25, Pin.OUT)


fridge_thermistor = ADC(Pin(35))
fridge_thermistor.width(ADC.WIDTH_9BIT)
freezer_thermistor = ADC(Pin(32))
freezer_thermistor.width(ADC.WIDTH_9BIT)

buzzer = PWM(Pin(23), duty=0, freq=1109)


alarm = Alarm(buzzer, ("overtemp", "door_open"), logger)


async def warn_door_open_timer():
    i = 0
    while not door_switch.switchstate:
        i += 1
        await asyncio.sleep(1)
        if i > 120:
            break
    if not door_switch.switchstate and light.value():
        alarm("door_open", Alarm.SOUNDING)
        while light.value():
            await asyncio.sleep(1)
        alarm("door_open", Alarm.OFF)


def light_on():
    light.on()
    asyncio.get_event_loop().create_task(warn_door_open_timer())


def wdt_feed():
    external_wdt.off()
    sleep_ms(10)  # blocking, so we don't end up in the wrong state
    external_wdt.on()


def oversample(adc, size):
    """Oversample by size, then return average dropping largest and smallest values."""
    samples = []
    for i in range(size):
        samples.append(adc.read())
    samples.sort()
    return round(sum(samples[1:-1]) / (len(samples) - 2))


def eval_poly(x, coeffs):
    """
    Evalulate polynomial.

    Coeffs should be passed little-endian, ie coeffs[0] = x^0.
    """
    y = 0
    for i, coeff in enumerate(coeffs):
        y += coeff * x ** i
    return y


def convert_cooler_thermistor():
    """
    Convert cooler thermistor reading to centigrade.

    Uses *stupid* 6-th order polynomial, because we have an 80MHz cpu.
    """
    raw_adc = oversample(freezer_thermistor, 10)
    coeffs = [
        30.283222924217103,
        -0.7448907815355763,
        0.007431971489726137,
        -4.422639819004203e-05,
        1.4675005113571936e-07,
        -2.512590532964693e-10,
        1.7228453832126142e-13,
    ]

    return eval_poly(raw_adc, coeffs), raw_adc


def convert_fridge_thermistor():
    raw_adc = oversample(fridge_thermistor, 10)
    return raw_adc, raw_adc
    # """Convert fridge thermistor reading to centigrade."""
    # coeff = [-5.70377895e-03, 3.37482693e-01, -7.88272201e00, 8.70994568e01]
    # return (
    #     coeff[0] * raw_adc ** 3
    #     + coeff[1] * raw_adc ** 2
    #     + coeff[2] * raw_adc
    #     + coeff[3]
    # )


sensors = {
    "fridge_temp": 0,
    "cooler_temp": -20,
    "freezer_temp": -20,
    "cooler_thermistor_temp": -20,
    "fridge_thermistor_temp": 0,
    "cooler_thermistor": None,
    "fridge_thermistor": None,
    "ext_temp": 20,
}


async def get_temps():
    sensors = {
        "freezer_temp": None,
        "fridge_temp": None,
        "ext_temp": None,
        "cooler_temp": None,
    }

    for _ in range(5):
        sens.convert_temp()
        await asyncio.sleep(1)
        for sensor in sensors:
            if sensors[sensor]:
                continue
            rom = getattr(config, sensor.replace("temp", "rom"))
            temp = sens.read_temp(rom)
            if temp != 85:
                sensors[sensor] = temp
        if all(x for _, x in sensors.items()):
            break
        await asyncio.sleep_ms(100)

    temp, raw = convert_fridge_thermistor()
    sensors["fridge_thermistor"] = raw
    sensors["fridge_thermistor_temp"] = temp
    temp, raw = convert_cooler_thermistor()
    sensors["cooler_thermistor"] = raw
    sensors["cooler_thermistor_temp"] = temp
    return sensors


async def temp_loop():
    global sensors
    while True:
        while not mock_temps:
            sensors = await get_temps()
            await asyncio.sleep(5)

        mocking_temps.set()

        while mock_temps:
            await asyncio.sleep(1)


door_switch = Switch(door_switch_pin)  # reads 1 at boot
door_switch.close_func(light_on)
door_switch.open_func(light.off)


def dummy(caller):
    logger.info("Dummy called by {}".format(caller))


enter_button = Pushbutton(enter_pin)
enter_button.press_func(dummy, ("Enter button",))
plus_button = Pushbutton(plus_pin)
plus_button.press_func(dummy, ("Plus button",))
minus_button = Pushbutton(minus_pin)
minus_button.press_func(dummy, ("Minus button",))


def switch_command(pin, state):
    if state:
        pin.on()
        logger.info("Turned {} on".format(repr(pin)))
    else:
        pin.off()
        logger.info("Turned {} off".format(repr(pin)))


def hardware_status():
    """Get hardware status."""
    status = {}
    names = ["compressor", "deep_freeze", "light", "fan", "heaters"]
    for i, thing in enumerate([compressor, deep_freeze, light, fan, heaters]):
        status[names[i]] = True if thing.value() else False
    status["mode"] = fridge.mode
    status["alarms"] = alarm.state
    status["runtime"] = clock.runtime()
    status["hal"] = repr(fridge_freezer)
    return status


def log_hardware_status():
    comms.log_msg(hardware_status())


async def check_hardware():
    """Check hardware once we have booted up."""
    await asyncio.sleep(120)
    if not door_switch():
        light.off()


fridge_freezer = FridgeFreezerHal(compressor, fan, deep_freeze, heaters, logger)


def get_wdt():
    """
    Get WDT.

    We do this here as the WDT is started as soon as created, and we
    don't want to start it when generating the coro object.
    """
    return WDT(timeout=20000)


async def set_wdt():
    wdt = get_wdt()
    fridge_freezer._wdt_feed = wdt.feed


def init(loop):
    loop.create_task(set_wdt())
    loop.create_task(temp_loop())
    fridge_freezer.start(loop)
    alarm.start(loop)


def self_test():
    from .self_test import run_tests

    run_tests()
