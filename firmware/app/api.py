import logging
import re
from sys import print_exception

import picoweb
import ujson as json
import uos as os

try:
    import uasyncio as asyncio
except ImportError:
    import asyncio

from . import clock, fridge, hal, comms

app = picoweb.WebApp(__name__)


@app.route("/api/status", methods=["GET"])
def status(req, resp):
    state = hal.hardware_status()
    state.update(hal.sensors)
    encoded = json.dumps(state)
    yield from picoweb.start_response(resp, content_type="application/json")
    yield from resp.awrite(encoded)


@app.route(re.compile("/api/mode/(manual|auto)"), methods=["GET", "PUT"])
def mode(req, resp):
    if req.method == "PUT":
        if req.url_match.group(1) == "manual":
            fridge.mode = "manual"
        elif req.url_match.group(1) == "auto":
            fridge.mode = "auto"
    encoded = json.dumps({"mode": fridge.mode})
    yield from picoweb.start_response(resp, content_type="application/json")
    yield from resp.awrite(encoded)


@app.route(re.compile("/api/hardware/(.+)/(on|off)"), methods=["PUT"])
def control_hardware(req, resp):
    periph = req.url_match.group(1)
    state = req.url_match.group(2)
    try:
        hal.fridge_freezer.set_state(periph, True if state == "on" else False)
    except Exception as e:
        print_exception(e)
    yield from status(req, resp)


@app.route(re.compile("/api/sound/(on|off)"), methods=["PUT"])
def sound(req, resp):
    if req.url_match.group(1) == "on":
        hal.sound()
    else:
        hal.sounding = False
    yield from status(req, resp)


@app.route(re.compile("/api/static/(.+)"))  # fails with /static, not sure why
def static(req, resp):
    print("Called static")
    fn = "static/{}".format(req.url_match.group(1))
    try:
        os.stat(fn)
    except OSError:
        yield from picoweb.start_response(resp, content_type="application/json")
        encoded = json.dumps({"Error": "File not found"})
        yield from resp.awrite(encoded)

    if fn.endswith("json"):
        yield from picoweb.start_response(resp, content_type="application/json")
    else:
        yield from picoweb.start_response(resp, content_type="text/plain")
    with open(fn) as f:
        yield from resp.awrite(f.read())


@app.route("/api/runtime")
def runtime(req, resp):
    runtime = clock.runtime()
    if runtime:
        h, rem = divmod(runtime, 3600)
        m, s = divmod(rem, 60)
        encoded = json.dumps({"runtime": "{:02}:{:02}:{:02}".format(h, m, s)})
    else:
        encoded = json.dumps({"runtime": None})
    yield from picoweb.start_response(resp, content_type="application/json")
    yield from resp.awrite(encoded)


@app.route("/api/self-test")
async def selftest(req, resp):
    fridge.mode = "manual"
    try:
        from . import self_test

        await self_test._self_test()
        await picoweb.start_response(resp, content_type="text/plain")
        with open("/static/test.log") as f:
            for line in f.readlines():
                await resp.awrite(line)
    except Exception as e:
        print_exception(e)
        await status(req, resp)


@app.route(re.compile("/api/silence/(.+)"), methods=["PUT"])
async def silence_alarm(req, resp):
    alarm = req.url_match.group(1)
    try:
        hal.alarm(alarm, hal.alarm.SILENCED)
        await status(req, resp)
    except Exception as e:
        print_exception(e)
        encoded = json.dumps({"Error", str(e)})
        await picoweb.start_response(resp, content_type="application/json")
        await resp.awrite(encoded)


@app.route("/api/repl")
async def fallback(req, resp):  # we should authenticate later
    with open("/fallback", "w") as f:
        f.write("")
    countdown()
    encoded = json.dumps({"status": "Falling back in 10s"})
    await picoweb.start_response(resp, content_type="application/json")
    await resp.awrite(encoded)


async def _fallback():
    print("Falling back in 10")
    await asyncio.sleep(10)
    from machine import reset

    reset()


def countdown():
    asyncio.get_event_loop().create_task(_fallback())


async def run_app():
    app.run(debug=True, host="0.0.0.0", port="80", log=logging)


def init(loop):
    loop.create_task(run_app())
