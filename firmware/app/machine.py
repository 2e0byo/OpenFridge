"""A mock machine class to allow testing.

This file should not end up on the target device, or bad things will
happen!
"""


class MockPinError(Exception):
    pass


class Pin:
    """Mock Pin() object."""

    IRQ_RISING = 1
    IRQ_FALLING = 2
    irqs = {-1: "unset", 1: "irq rising", 2: "irq falling"}

    WAKE_LOW = 4
    WAKE_HIGH = 5
    wakes = {-1: "unset", 4: "wake low", 5: "wake high"}

    PULL_DOWN = 1
    PULL_UP = 2
    PULL_HOLD = 4
    pulls = {-1: "unset", 1: "pull down", 2: "pull up", 4: "pull hold"}

    IN = 1
    OUT = 3
    OPEN_DRAIN = 7
    modes = {-1: "unset", 1: "in", 3: "out", 7: "open_drain"}

    def __repr__(self):
        state = []
        state.append("mode: {}".format(self.modes[self.mode]))
        state.append("pull: {}".format(self.pulls[self.pull]))
        state.append("wake: {}".format(self.wakes[self.wake]))
        state.append("irq: {}".format(self.irqs[self._irq]))

        return "Pin object attached to pin {} with state {}".format(
            self.id, " and ".join(state)
        )

    def __init__(self, id, mode=-1, pull=-1, *, value=None, drive=None, alt=None):
        self.id = id
        self.mode = -1
        self.pull = -1
        self._value = 0
        self.wake = -1
        self._irq = -1
        self._output_buffer = None
        self.init(mode, pull, value=value, drive=drive, alt=alt)

    def init(self, mode=-1, pull=-1, *, value=None, drive=None, alt=None):
        """Re-initialise the pin using the given parameters.

        Only those arguments that are specified will be set. The rest of
        the pin peripheral state will remain unchanged
        """
        if mode != -1:
            self.mode = mode
            if self._output_buffer is not None and self.mode == self.OUT:
                self.value(self._output_buffer)
                self._output_buffer = None
        if pull != -1:
            self.pull = pull
        if value:
            self.value(value)

    def value(self, x=-1):
        if x != -1:
            if self.mode == self.OUT:
                self._value = 1 if x else 0
            else:
                self._output_buffer = 1 if x else 0
        else:
            return self._value

    def __call__(self, x=-1):
        return self.value(x)

    def on(self):
        return self.value(1)

    def off(self):
        return self.value(0)

    def irq(self):
        raise NotImplementedError

    def write(self, x):
        """Write to pin if input."""
        if self.mode != self.IN:
            raise MockPinError("Can only write to input pins.")
        else:
            self._value = 1 if x else 0
