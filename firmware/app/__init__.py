import uos

if "fallback" in uos.listdir("/"):
    uos.remove("/fallback")
    raise Exception("Falling back to repl this once")


def start(logger):
    print("Starting up")

    import gc
    from time import sleep

    import machine
    import uasyncio as asyncio

    reset_causes = (
        "Power on",
        "Hard reset",
        "WDT reset",
        "Deepsleep reset",
        "Soft reset",
    )

    gc.enable()

    print("Loading Hal")
    from . import hal

    hal.wdt_feed()  # buy us some time to init

    print("Loading clock")
    from . import clock

    print("Loading comms")
    from . import comms

    gc.collect()

    logger.addHandler(comms.handler)
    reset_cause = reset_causes[machine.reset_cause() - 1]
    logger.info("Booting up, reason is {}".format(reset_cause))

    print("Loading fridge")
    from . import fridge

    gc.collect()

    print("Initialising...")
    loop = asyncio.get_event_loop()
    hal.init(loop)
    fridge.init(loop)
    clock.init(loop)

    gc.collect()

    print("Loading api")
    from . import api

    print("Initialising...")
    api.init(loop)
    gc.collect()
    gc.threshold(gc.mem_free() // 4 + gc.mem_alloc())

    logger.info("Everything started.")

    try:
        loop.run_until_complete(comms.main(comms.client))
    finally:
        comms.client.close()  # Prevent LmacRxBlk:1 errors
        print("Loop ended")
        if not comms.cancelled:
            sleep(60)
            machine.reset()
        else:
            raise Exception("Falling back")
