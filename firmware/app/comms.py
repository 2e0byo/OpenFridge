import gc
import logging
from sys import stdout

import uasyncio as asyncio

from . import clock
from .config import config, fridge_name, log_topic, sensor_topic
from .mqtt_as import MQTTClient
from primitives.queue import Queue


class MQTTHandler(logging.StreamHandler):
    """A handler class to log to mqtt."""

    def __init__(self, send_func):
        super().__init__()
        self.send_func = send_func

    def emit(self, record):
        """Publish record to self.topic."""
        if record.levelno >= self.level:
            msg = self.formatter.format(record)
            self.send_func(msg)


msg_id = 0

log_queue = Queue(maxsize=5)


async def send_msg(msg, topic):
    await client.publish(topic, str(msg), qos=1)


async def send_readings(readings):
    await client.publish(sensor_topic, readings, qos=1)


async def main(client):
    from . import hal

    await client.connect()
    clock.sync_clock()
    print("Starting client loop")
    asyncio.get_event_loop().create_task(log_loop())
    while True:
        status = hal.sensors.copy()
        status["compressor"] = int(hal.compressor.value())
        status["deep_freeze"] = int(hal.deep_freeze.value())
        status["light"] = int(hal.light.value())
        status["fan"] = int(hal.fan.value())
        status["heater"] = int(hal.heaters.value())
        status["ram_free"] = gc.mem_free()
        status["fridge_name"] = fridge_name
        status["timestamp"] = clock.clockstr()
        status["runtime"] = clock.runtime()
        msg = (
            "{fridge_name},{fridge_temp},{freezer_temp},{ext_temp},"
            "{fridge_thermistor},{cooler_thermistor},{fridge_thermistor_temp},"
            "{cooler_temp},{compressor},{deep_freeze},{light},{fan},{heater},"
            "{ram_free},{timestamp},{runtime}".format(**status)
        )
        await send_readings(msg)
        await asyncio.sleep(5)


async def log_loop():
    while True:
        msg = await log_queue.get()
        await send_msg(msg, log_topic)
        await asyncio.sleep(0)  # don't force priority


async def _log_msg(msg):
    await log_queue.put(msg)


def log_msg(*args):
    msg = " ".join([str(i) for i in args])
    asyncio.get_event_loop().create_task(_log_msg(msg))


config["clean"] = False

MQTTClient.DEBUG = False  # Optional: print diagnostic messages
client = MQTTClient(config)

handler = MQTTHandler(log_msg)
handler.setFormatter(
    logging.Formatter("%(asctime)s - %(name)s -  %(levelname)s: %(message)s")
)
handler.setLevel(logging.INFO)
sh = logging.StreamHandler(stdout)
sh.setFormatter(
    logging.Formatter("%(asctime)s - %(name)s - %(levelname)s: %(message)s")
)
