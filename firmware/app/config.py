# config.py Local configuration for mqtt_as demo programs.
from secrets import wifi_PSK, wifi_SSID

from .mqtt_as import config

config["server"] = "voyage.lan"

# Not needed if you're only using ESP8266
config["ssid"] = wifi_SSID
config["wifi_pw"] = wifi_PSK

log_topic = "kitchen/fridge/log"
command_topic = "kitchen/fridge/CommandControl"
sensor_topic = "kitchen/fridge/sensors"
fridge_name = "OpenFridge"
ext_rom = bytearray(b"(\x1f\xfa\xfa\x02\x00\x00_")
cooler_rom = bytearray(b"(\xd1\xca\x07\xd6\x01<\xe5")
freezer_rom = bytearray(b"(-\xa1\x07\xd6\x01<\x8b")
fridge_rom = bytearray(b"(\xb3\xa1\x07\xd6\x01<)")
