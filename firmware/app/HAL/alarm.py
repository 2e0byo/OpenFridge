import uasyncio as asyncio


class Alarm:
    """Flexible alarm class with different conditions and silencing."""

    FREQS = [440, 622, 659, 340]
    SILENCED = -1
    SOUNDING = 1
    OFF = 0

    def __init__(self, buzzer, alarms, logger=None):
        self.buzzer = buzzer
        assert len(alarms) < 5
        self.state = {k: self.OFF for k in alarms}
        self.freqs = {k: self.FREQS[i] for i, k in enumerate(alarms)}
        self._sounding_freqs = []
        self.logger = logger

    def silence_alarm(self, alarm):
        self.state[alarm] = self.SILENCED

    def __call__(self, alarm, x=None):
        """Get or set alarm."""
        if x is None:
            return self.state[alarm]
        elif x is not None:
            if x == self.SOUNDING:
                if self.state[alarm] != self.SILENCED:
                    self.state[alarm] = self.SOUNDING
            else:
                self.state[alarm] = x

    def sound(self, alarm):
        """Sound even if silenced."""
        self.state[alarm] = self.SOUNDING

    def cancel_all(self):
        for alarm in self.state:
            self.state[alarm] = self.OFF

    def silence_all(self):
        for alarm in self.state:
            if self.state[alarm] == self.SOUNDING:
                self.state[alarm] = self.SILENCED

    async def sounding_loop(self):
        if self.logger:
            self.logger.debug("Started alarm sounding loop")
        while True:
            for alarm, val in self.state.items():
                if val == self.SOUNDING:
                    self.buzzer.freq(self.freqs[alarm])
                    self.buzzer.duty(512)
                    await asyncio.sleep_ms(512)
            self.buzzer.duty(0)
            await asyncio.sleep_ms(512)

    def start(self, loop):
        loop.create_task(self.sounding_loop())

    def __repr__(self):
        return "Alarm with status {}".format(repr(self.state))

    def __str__(self):
        return self.__repr__()
