try:
    import uasyncio as asyncio
except:
    import asyncio
from primitives.delay_ms import Delay_ms


class Conflict:
    """Class to represent a conflict between states."""

    def __init__(self, conflicting_states: dict, name: str, state: bool):
        self.conflicting_states = conflicting_states
        self.name = name
        self.state = state

    def __call__(self, proposed_state) -> bool:
        """See if proposed state is forbidden.  We OR forbidden states (for now)."""
        if proposed_state[self.name] is not self.state:
            return False  # we don't apply
        for name, val in proposed_state.items():
            try:
                if self.conflicting_states[name] == val:
                    return True
            except KeyError:
                pass
        return False

    def __str__(self):
        return "Conflict obj for {}={} with states {}".format(
            self.name, self.state, str(self.conflicting_states)
        )

    def __repr__(self):
        return self.__str__()


class HAL:
    """Class to do the actual controlling of hardware.

    This class has methods to command a particular *state*, which it tries to
    realise.  In this way forbidden hardware states are definitely avoided."""

    def __init__(self, outputs, names, wdt_feed=None, logger=None):
        """Setup a hal to control outputs with given names."""
        self.enabled = True
        self.loop = None
        self._wdt_feed = wdt_feed
        self._state = {
            n: True if outputs[i].value() else False for i, n in enumerate(names)
        }
        self._outputs = {n: outputs[i] for i, n in enumerate(names)}
        self._downtimes = {}
        self._conflicts = []
        self._sleeping = {n: False for n in names}
        self._timers = []
        self.logger = logger
        self.masked_hardware = []
        self._masked_outputs = {n: 0 for n in names}
        self.debugging = False

    def __repr__(self):
        return (
            "HAL object, state: {}, outputs: {}, downtimes: {}, conflicts: {}"
            ", sleeping: {}, timers: {}".format(
                self._state,
                {k: v() for k, v in self._outputs.items()},
                self._downtimes,
                self._conflicts,
                self._sleeping,
                [x.running() for x in self._timers],
            )
        )

    def __str__(self):
        return self.__repr__()

    def set_state(self, name, val):
        self._state[name] = True if val else False
        return self._state[name]

    def get_state(self, name):
        return self._state[name]

    def __call__(self, name, val=-1):
        if val != -1:
            return self.set_state(name, val)
        else:
            return self.get_state(name)

    def get_output_state(self, name):
        if name not in self.masked_hardware:
            return self._outputs[name].value()
        else:
            return self._masked_outputs[name]

    def achieved_state(self, name):
        """Whether hardware reflects desired state."""
        return self.get_output_state(name) == self.get_state(name)

    @property
    def engaged(self):
        """Whether we are controlling the hardware or not."""
        return self.enabled and self.loop

    @engaged.setter
    def engaged(self, val: bool):
        self.enabled = True if val else False
        if self.enabled and not self.loop:
            self.start()
        if not self.enabled:
            for timer in self._timers:
                timer.stop()
            self._timers = []
            for k in self._sleeping:
                self._sleeping[k] = False

    def reset(self):
        for k in self._state:
            self._state[k] = False

    def add_conflict(self, name, val, conflicting_states):
        """Add conflicting states to the HAl.

        The HAL will not command an output until the conflicting conditions
        have gone away. Resolving conflicting states is the job of user
        code.


        Args:
          name: The name of the output to set the conflict for.
          val: The state in question.
          conflicting_states: {name: state} or fn

        """
        if name not in self._state:
            raise Exception(
                "{} not in hal's names ({})".format(name, self._state.keys())
            )
        if isinstance(conflicting_states, dict):
            self._conflicts.append(Conflict(conflicting_states, name, val))
        else:
            self._conflicts.append(conflicting_states)

    def add_downtime(self, name, length):
        """Add a downtime for an output.

        The output will not be switched on for downtime seconds after it has
        been switched off.

        Args:
          name: The name of the output to set the downtime for.
          length: The downtime length in seconds.

        """
        self._downtimes[name] = length

    def start(self, loop):
        self.loop = loop
        self.loop.create_task(self.hardware_control_loop())

    def _unset_downtime_flag(self, name):
        self._sleeping[name] = False
        return True

    async def _start_downtime_timer(self, name, duration):
        if self.logger:
            self.logger.debug(
                "Starting downtime for {} of duration {}".format(name, duration)
            )
        self._sleeping[name] = True
        self._timers.append(
            Delay_ms(self._unset_downtime_flag, (name,), duration=duration * 1000)
        )
        self._timers[-1].trigger()
        self._timers = [x for x in self._timers if not x.rvalue()]

    async def _set_output(self, name, val):
        if self.logger:
            self.logger.debug("Setting {} to state {}".format(name, val))
        if name not in self.masked_hardware:
            self._outputs[name].value(val)
        else:
            self._masked_outputs[name] = val
        try:
            if not val:
                await self._start_downtime_timer(name, self._downtimes[name])
        except KeyError:
            pass

    @staticmethod
    def check_conflict(conflict, proposed_state):
        return conflict(proposed_state)

    async def hardware_control_loop(self):
        msgd_conflict = []
        msgd_sleeping = []
        while True:
            while not self.enabled:
                if self.debugging:
                    self.logger.debug("Not enabled so loop paused.")
                await asyncio.sleep(1)
                if self._wdt_feed:
                    self._wdt_feed()

            while self.enabled:
                for name, val in self._state.items():
                    if self.achieved_state(name):
                        continue

                    proposed_state = self._state.copy()
                    proposed_state[name] = val
                    conflicts = any(
                        self.check_conflict(c, proposed_state) for c in self._conflicts
                    )
                    if conflicts:
                        if not self.logger or name in msgd_conflict:
                            continue
                        if not self.debugging:
                            msgd_conflict.append(name)
                        self.logger.debug(
                            "Not setting {} to state {} due to conflict".format(
                                name, val
                            )
                        )
                        if self.debugging:
                            self.logger.debug(repr(self._state))

                    elif self._sleeping[name]:
                        if not self.logger or name in msgd_sleeping:
                            continue
                        if not self.debugging:
                            msgd_sleeping.append(name)
                        self.logger.debug(
                            "Not setting {} to state {} due to enforced downtime".format(
                                name, val
                            )
                        )

                    else:
                        await self._set_output(name, val)
                        if name in msgd_sleeping:
                            msgd_sleeping.remove(name)
                        if name in msgd_conflict:
                            msgd_conflict.remove(name)

                if self._wdt_feed:
                    self._wdt_feed()
                await asyncio.sleep(1)


class FridgeError(Exception):
    pass


class FridgeFreezerHal(HAL):
    """Fridge freezer hal."""

    def __init__(self, compressor, fan, deep_freeze, heaters, logger=None):
        """We take them as params to make testing easier."""
        super().__init__(
            [compressor, fan, deep_freeze, heaters],
            ["compressor", "fan", "deep_freeze", "heaters"],
            logger=logger,
        )

        self.add_conflict(
            "heaters", True, {"fan": True, "compressor": True, "deep_freeze": True}
        )
        self._refridgerating = False
        self._freezing = False
        self._defrosting = False
        self.add_downtime("compressor", 5 * 60)

    def __repr__(self):
        hardware = super().__repr__()
        state = []
        if self.freezing:
            state.append("freezing")
        if self.refridgerating:
            state.append("refridgerating")
        if self.defrosting:
            state.append("defrosting")
        return "FridgeFreezer, currently {}.  (Hardware: {})".format(
            " and ".join(state), hardware
        )

    def __str__(self):
        return self.__repr__()

    @property
    def freezing(self):
        return self._freezing

    @freezing.setter
    def freezing(self, val):
        if val and self.defrosting:
            raise FridgeError("Not allowed to freeze when defrosting")
        if self._freezing is not val:
            if self.logger:
                self.logger.info("Turning freezing {}".format("on" if val else "off"))
        self._freezing = val
        if not val:
            self.set_state("fan", False)
            if not self._refridgerating:
                self.set_state("compressor", False)
                self.set_state("deep_freeze", False)
        else:
            self.set_state("fan", True)
            self.set_state("compressor", True)

    @property
    def refridgerating(self):
        return self._refridgerating

    @refridgerating.setter
    def refridgerating(self, val):
        if val and self.defrosting:
            raise FridgeError("Not allowed to refridgerate when defrosting")
        if val is not self._refridgerating:
            if self.logger:
                self.logger.info(
                    "Turning refridgerating {}".format("on" if val else "off")
                )
        self._refridgerating = val
        if not val and not self.freezing:
            self.set_state("compressor", False)
            self.set_state("deep_freeze", False)
        else:
            self.set_state("compressor", True)

    @property
    def defrosting(self):
        return self._defrosting

    @defrosting.setter
    def defrosting(self, val):
        if val and (self.freezing or self.refridgerating):
            raise FridgeError(
                "Not allowed to defrost when state is {}".format(repr(self))
            )
        if val is not self._defrosting:
            if self.logger:
                self.logger.info("Turning defrosting {}".format("on" if val else "off"))
        self._defrosting = val
        if val:
            self.set_state("heaters", True)
        else:
            self.set_state("heaters", False)

    def reset(self):
        self.defrosting = False
        self.freezing = False
        self.refridgerating = False
        super().reset()
